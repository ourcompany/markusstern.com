﻿using System.Threading.Tasks;
using MongoDB.Driver;
using ourCompany.cms.Data;
using Website.App.Routing.Views;
using Website.Layouts;

namespace Website.Views
{
    public class Startpage : View<Default>
    {
        public override async Task<bool> Render()
        {
            var root = await Database.RootAsync();
            var first = await Database.Find<Model.ExampleNode>().FirstOrDefaultAsync();


            //var root = await MongoContext.GetRootAsync(Layout.Database);

            //Layout.Menu.Tag("div", "last mod : " + root.LastUpdate.ToString());

            //Layout.Menu.Tag("div", "thesaurus : " + Resources.Thesaurus.Romain + " thread " + Thread.CurrentThread.CurrentCulture);

            //var children = await root.FindChildrenAsync();
            //foreach (var child in children)
            //{
            //    Layout.Menu.Parse("div");
            //    Layout.Menu.Tag("h1", "child " + child.Name);
            //    if (child is A)
            //    {
            //        var Achild = child as A;
            //        Layout.Menu.Parse("div");
            //        Layout.Menu.Tag("h1", "A Child " + Achild.Title);
            //        Layout.Menu.AddAttribute("href", "/" + Layout.Lang + "/" + Achild.UID);
            //        Layout.Menu.Tag("a", "link to " + Achild.Title);
            //        foreach (var img in Achild.Pictures)
            //        {
            //            Layout.Menu.AddAttribute("src", img.Src);
            //            Layout.Menu.Tag("img");
            //        }

            //        Layout.Menu.Parse("/div");
            //        var leaves = await child.FindLeavesAsync();
            //        foreach (var leaf in leaves)
            //        {
            //            if (leaf is A.TextPanel)
            //            {
            //                var ATextPanelleaf = leaf as A.TextPanel;
            //                Layout.Menu.Parse("div");
            //                Layout.Menu.Tag("h1", "A.TextPanel Leaf " + ATextPanelleaf.HTML);
            //                Layout.Menu.Parse("/div");
            //            }
            //        }
            //        Layout.Menu.Parse("/div");
            //    }
            //    Layout.AddNodeDependency(child.DBRef);
            //}
            //Layout.AddNodeDependency(root.DBRef);
            return true;
        }
    }
}