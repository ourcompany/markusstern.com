﻿using System.Threading.Tasks;
using Website.App.Routing;
using Website.App.Routing.Views;
using Website.Layouts;
using Website.Model;

namespace Website.Views
{
    public class ContentView : View<Default>
    {
        [FromRoute]
        public ExampleNode Node { get; set; }

        [FromRoute]
        public Child SubNode { get; set; }

        public override async Task<bool> Render()
        {
            //var parent = await Node.FindParentAsync<Root>();

            var requestContext = Context;

            var route = Route;

            Layout.Menu.Tag("div", Node.Title);
            return true;
        }
    }
}