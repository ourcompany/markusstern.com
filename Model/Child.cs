using ourCompany.cms.Model;
using ourCompany.cms.Widgets;

namespace Website.Model
{
    public class GrandChild : Node
    {


        [Text(Legend = "title")]
        public string Title { get; set; } = "default title";

        [UID(Legend = "uid", Base = "Title", Index = true)]
        public string UID { get; set; }
    }
    public class Child : Node, IParent<GrandChild>
    {
        public override int? Max => 3;

        [Text(Legend = "title")]
        public string Title { get; set; } = "default title";

        [UID(Legend = "uid", Base = "Title", Index = true)]
        public string UID { get; set; }
    }
}