using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using ourCompany.cms.Attributes;
using ourCompany.cms.Model;
using ourCompany.cms.Widgets;

namespace Website.Model
{

    public class ObjectClass
    {

        [Text(Legend = "title")]
        public string Title { get; set; } = "default title";

        [CMSIgnore]
        public static string CreateLegend { get; } = "creation of the node";

    }

    public class ExampleNode : Node, IParent<Child>
    {
        public override int? Max => 3;
        public override MenuIcon? EditIcon => MenuIcon.Trash;

        public override string EditLegend => "yellow";

        [Online(Legend = "online")]
        public bool Online => Published;

        [Text(Legend = "title")]
        public string Title { get; set; } = "default title";


        [LatLng(Legend = "test latlng")]
        public double[] Testlatlng { get; set; }

        [Slider(Legend = "test slider", Min = -500, Max = 500, Step = 0.1)]
        public double TestSlider { get; set; }

        [Slider(Legend = "test slider 2")]
        public double TestSlider2 { get; set; }

        [Slider(Legend = "test slider 3", Step = 0.1)]
        public double TestSlider3 { get; set; }

        [Info(Legend = "test info")]
        public string Testinfos
        {
            get
            {
                return string.Join("<br /> ", MongoDb.GetCollection<Child>().FindSync<Child>(new BsonDocument()).ToList().Select(c => c.Title));
            }

        }

        [Number(Legend = "test int")]
        public int Testint { get; set; }

        [Number(Legend = "test float", Min = -5, Max = 5)]
        public double Testfloat { get; set; }

        [Number(Legend = "test long", Min = 3.5, Max = 12.1)]
        public long Testlong { get; set; }

        //[Number(Legend = "test decimal")]
        //public decimal Testdecimal { get; set; }

        [Date(Legend = "test date", Pick = Date.PickOptions.DateTime)]
        public DateTime TestDate { get; set; } = DateTime.Now;


        [Files(Legend = "Files")]
        public FileRef[] Files { get; set; }

        [UID(Legend = "uid", Base = "Title", Index = true)]
        public string UID { get; set; }

        [Tags(Legend = "title")]
        public string[] Tags { get; set; }

        [ourCompany.cms.Widgets.Action("action !")]
        public Func<ApiController, HttpResponseMessage> action => (controller) =>
         {

             var file = HttpContext.Current.Server.MapPath("~/human.txt"); //location of the template file
             var result = new HttpResponseMessage(HttpStatusCode.OK);
             var stream = new FileStream(file, FileMode.Open);
             result.Content = new StreamContent(stream);
             result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
             result.Content.Headers.ContentDisposition.FileName = $"{DateTime.UtcNow.ToString("yyyy-MM-dd")}.txt";
             result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/download");
             return result;

         };

        public enum TestEnum
        {
            [Description("legend2")]
            Value2 = 1, // 20
            [Description("legend3")]
            Value3 = 2, // 21
            [Description("legend4")]
            Value4 = 4, // 22
            [Description("legend6")]
            Value6 = 8 // 23
        }

        [Enumeration]
        public TestEnum Enum { get; set; }

        //[Checkbox]
        //public bool Checkbox { get; set; }

        //[Color]
        //public string Color { get; set; }

        //[Date]
        //public DateTime DateTime { get; set; } = DateTime.Now;

        [HTML]
        [HTML.CustomStyles(Path = Configuration.InnerHTML.CustomCSSPath, StyleFormats = Configuration.InnerHTML.StyleFormats)]
        //[HTML.Config(Key = "toolbar", Value = "bold,italic,underline")]
        public string TextEditor
        {
            get;
            set;
        }


        //public override MenuIcon? Icon => Exists ? MenuIcon.NewPage : base.Icon;

        //public override string Legend => Exists ? Title : "create A";

        //[References]
        //[References.Nodes(
        //    GroupTitle = "group A",
        //    Type = typeof(Example),
        //    Filter = "{ $and : [{Title: { $ne : null } },{Title: { $ne : null } }]}",
        //    SortKey = "Title",
        //    Legend = "Title"
        //)]
        //public MongoDBRef[] References { get; set; }


        [Pictures(LegendFormat = Picture.PictureLegendFormat.HTML, HasLink = true)]
        public PictureRef[] Pictures { get; set; } = new PictureRef[] { };

        [Picture]
        public PictureRef Preview { get; set; }

        // Reference Widget
        [Reference]
        [Reference.Nodes(
            GroupTitle = "group name",
            Type = typeof(ExampleNode),
            Filter = "{ $and : [{Title: { $ne : null } },{Title: { $ne : '' } }]}",
            SortKey = "Title",
            Legend = "Title"
        )]
        public MongoDBRef Reference { get; set; }



        //[Select]
        //[Select.Values("key 1", "value1", "key 2", "value2", "key 3", "value 3", GroupTitle = "Group name")]
        //[Select.Nodes(
        //    GroupTitle = "group A",
        //    Type = typeof(Example),
        //    Filter = "{ $and : [{Title: { $ne : null } },{Title: { $ne : null } }]}",
        //    SortKey = "Title",
        //    Legend = "ReferenceLegend",
        //    Value = "UID"
        //)]
        //public string SelectWidget { get; set; }

        //[Switch]
        //[Switch.Color(Active = Switch.Color.Theme.Blue, Inactive = Switch.Color.Theme.Red)]
        //public bool SwitchWidget
        //{
        //    get; set;
        //}



        //[Video]
        //public VideoRef Video { get; set; }

        [List]
        [List.Values("key 1", "value1", "key 2", "value2", "key 3", "value 3", GroupTitle = "Group 1")]
        [List.Nodes(
            GroupTitle = "group A",
            Type = typeof(ExampleNode),
            Filter = "{ $and : [{Title: { $ne : null } },{Title: { $ne : null } }]}",
            SortKey = "Title",
            Value = "UID",
            Legend = "Title"
        )]
        public string[] WidgetList
        {
            get; set;
        }

        public class TextPanel : Leaf
        {

            public override string EditLegend => Title;

            [HTML]
            public string HTML { get; set; }

            [Text]
            public string Title { get; set; }

            //public override MenuIcon? Icon => Exists ? MenuIcon.NewPage : base.Icon;
        }

        [Embed]
        public IEnumerable<EmbedTest> Embeds { get; set; }



        public class EmbedTest
        {
            [Text]
            public string Title { get; set; }

            [HTML]
            public string HTML { get; set; }

        }
    }
}