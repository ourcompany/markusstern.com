﻿using MongoDB.Bson.Serialization.Attributes;
using ourCompany.cms.Model;

namespace Website.Model
{
    [BsonIgnoreExtraElements]
    public class Root :
        RootNode,
        IParent<ExampleNode>,
        IParent<ObjectClass>,
        IParent<Child>,
        IDetachedParent<TestDetachedNode>
    {
    }
}