﻿using ourCompany.cms.Model;
using ourCompany.cms.Widgets;

namespace Website.Model
{
    public class TestDetachedNode : DetachedNode
    {
        [Text]
        public string Title { get; set; }

        public override IDetachedDefinition DetachedDefinition { get; set; } = new DetachedDefinition { MenuLegend = "Detached node list" };

        public override string EditLegend => "<div>" + Title + "</div>";

    }
}