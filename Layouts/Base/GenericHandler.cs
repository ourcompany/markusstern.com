﻿using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using ourCompany.cms.Data;
using Website.App.Localization;
using Website.App.Routing;
using Website.Tools;

namespace Website.Layouts.Base
{
    public interface IGenericHandler : IHttpHandler
    {
        Task ProcessRequestAsync(HttpContext context);
    }

    public abstract class GenericHandlerBase : IGenericHandler, IRouted
    {
        private string _Lang;

        public DB Database { get; set; }

        public virtual bool IsReusable
        {
            get
            {
                return false;
            }
        }

        [FromRoute(Name = Router.LANG_ROUTE_ATTRIBUTE)]
        public string Lang
        {
            get
            {
                return _Lang;
            }
            set
            {
                try
                {
                    Thread.CurrentThread.CurrentCulture =
                        Thread.CurrentThread.CurrentUICulture =
                        CultureInfo.DefaultThreadCurrentCulture =
                        CultureInfo.DefaultThreadCurrentUICulture =
                        new CultureInfo(value);
                }
                catch
                {
                    Thread.CurrentThread.CurrentCulture =
                        Thread.CurrentThread.CurrentUICulture =
                        CultureInfo.DefaultThreadCurrentCulture =
                        CultureInfo.DefaultThreadCurrentUICulture =
                        CultureInfo.InvariantCulture;
                }
                _Lang = value;
            }
        }

        public Language Language { get; set; }



        public abstract Task<bool> ProcessAsync(HttpContext context);

        public void ProcessRequest(HttpContext context)
        {
            AsyncHelper.RunSync(() => ProcessRequestAsync(context));
        }

        public async Task ProcessRequestAsync(HttpContext context)
        {
            context.Response.ContentType = "application/json";


            Lang = Lang ?? Router.Languages.First().IsoCode;
            Language = Router.Languages.First(l => l.IsoCode == Lang);
            Database = DB.FromLang(Lang);

            await ProcessAsync(context);
        }
    }
}