﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Optimization;
using System.Web.UI;
using MongoDB.Driver;
using ourCompany.cms.Data;
using Website.App.Localization;
using Website.App.Routing;
using Website.App.Routing.Routes;

namespace Website.Layouts.Base
{
    public static class PageBaseExtensions
    {
        public static Route<T> BodyClass<T>(this Route<T> route, params string[] classes) where T : PageBase, new()
        {
            return route
                .Calls((p, r, c, v) =>
                {
                    p.BodyClasses.AddRange(classes);
                });
        }

        public static Route<T> Stylesheet<T>(this Route<T> route, params string[] paths) where T : PageBase, new()
        {
            return route
                .Calls((p, r, c, v) =>
                {
                    foreach (var path in paths)
                        p.Stylesheets.Add(path);
                });
        }

        public static Route<T> JSRequires<T>(this Route<T> route, params string[] paths) where T : PageBase, new()
        {
            return route
                .Calls((p, r, c, v) =>
                {
                    p.JSRequire.AddRange(paths);
                });
        }

        public static Route<T> StylesBundles<T>(this Route<T> route, params Bundle[] bundles) where T : PageBase, new()
        {
            return route
                .Calls((p, r, c, v) =>
                {
                    p.StyleBundles.AddRange(bundles);
                });
        }

        public static Route<T> ScriptBundles<T>(this Route<T> route, Bundle[] bundles) where T : PageBase, new()
        {
            return route
                .Calls((p, r, c, v) =>
                {
                    p.ScriptBundles.AddRange(bundles);
                });
        }
    }

    [PartialCaching(604800)]
    public class PageBase : Page, IRouted
    {
        protected List<string> _BodyClasses;
        public List<string> BodyClasses => _BodyClasses ?? (_BodyClasses = new List<string>());

        protected List<string> _JSRequire;
        public List<string> JSRequire => _JSRequire ?? (_JSRequire = new List<string>());

        protected List<Bundle> _ScriptBundles;
        public List<Bundle> ScriptBundles => _ScriptBundles ?? (_ScriptBundles = new List<Bundle>());

        protected List<string> _Stylesheets;
        public List<string> Stylesheets => _Stylesheets ?? (_Stylesheets = new List<string>());

        protected List<Bundle> _StyleBundles;
        public List<Bundle> StyleBundles => _StyleBundles ?? (_StyleBundles = new List<Bundle>());

        private string _Lang;

        [FromRoute(Name = Router.LANG_ROUTE_ATTRIBUTE)]
        public string Lang
        {
            get
            {
                return _Lang;
            }
            set
            {
                try
                {
                    Thread.CurrentThread.CurrentCulture =
                        Thread.CurrentThread.CurrentUICulture =
                        CultureInfo.DefaultThreadCurrentCulture =
                        CultureInfo.DefaultThreadCurrentUICulture =
                        new CultureInfo(value);
                }
                catch
                {
                    Thread.CurrentThread.CurrentCulture =
                        Thread.CurrentThread.CurrentUICulture =
                        CultureInfo.DefaultThreadCurrentCulture =
                        CultureInfo.DefaultThreadCurrentUICulture =
                        CultureInfo.InvariantCulture;
                }

                _Lang = value;
                Language = Router.Languages.FirstOrDefault(l => l.IsoCode == _Lang);
            }
        }

        public int CacheDuration = 10000;

        public List<string> CacheDependencies;

        public Language Language { get; private set; }

        private bool? _CacheControl;

        public bool CacheControl
        {
            get
            {
                if (_CacheControl.HasValue)
                    return _CacheControl.Value;
#if !DEBUG
                return true;
#endif
                return false;
            }
            set
            {
                _CacheControl = value;
            }
        }

        public PageBase()
        {
            Init += InitPage;
            PreRender += PreRenderPage;
            Load += LoadPage;
            LoadComplete += LoadCompletePage;
        }

        private void PreRenderPage(object sender, EventArgs e)
        {
        }

        protected void InitPage(object sender, EventArgs e)
        {
            EnableViewState = false;
            //MongoContext = Context.GetOwinContext().Get<MongoContext>();
            //Lang = Lang ?? Configuration.Languages.First().IsoCode;
            //Language = Configuration.Languages.First(l => l.IsoCode == Lang);
            //Database = Context.GetOwinContext().Get<MongoDb>();
        }

        //public RequestContext Context { get; set; }



        public DB Database { get; set; }

        private List<MongoDBRef> NodeDependencies { get; set; } = new List<MongoDBRef>();

        public void AddNodeDependency(MongoDBRef item)
        {
            NodeDependencies.Add(item);
        }

        private bool _CMSDependency = false;

        public void AddCMSDependency()
        {
            _CMSDependency = true;
        }

        protected void LoadPage(object sender, EventArgs e)
        {
        }

        protected void LoadCompletePage(object sender, EventArgs e)
        {
            if (CacheControl)
                _AddCacheControl();
        }

        private void _AddCacheControl()
        {
            if (CacheDuration <= 0) return;
            if (CacheDependencies != null && CacheDependencies.Count > 0)
                Response.AddFileDependencies(CacheDependencies.ToArray());

            var custom = "cms";
            if (!_CMSDependency && NodeDependencies.Any())
                custom += ":" + string.Join(",", NodeDependencies.Select(n => n.CollectionName + "/" + n.Id.ToString()));

            custom += ";port";

            var outputCacheSettings = new OutputCacheParameters { Duration = CacheDuration, VaryByParam = "*", VaryByCustom = custom };
            InitOutputCache(outputCacheSettings);
        }
    }
}