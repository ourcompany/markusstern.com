﻿<%@ Page Async="true" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Website.Layouts.Default" %>

<%@ Register TagPrefix="asp" Namespace="ourCompany" Assembly="ourCompany.CSSControl" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<%=Language.IsoCode %>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<%=Language.IsoCode %>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<%=Language.IsoCode %>"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<%=Language.IsoCode %>">
<!--<![endif]-->
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <asp:PlaceHolder runat="server" ID="MetasPH" />
    <asp:PlaceHolder runat="server" ID="StylesPH" />
</head>
<body runat="server" id="body">
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div id="container">
        <asp:CSSControl runat="server" ID="Menu" />
        <asp:CSSControl runat="server" ID="Content" />
    </div>

    <script async defer data-main="<%=MainJSPath %>" src="<%=RequireJSPath %>"></script>
    <asp:PlaceHolder runat="server" ID="ScriptsPH" />
    <script>
        window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
        ga('create', 'UA-XXXXX-Y', 'auto'); ga('send', 'pageview');
        // TODO: Create Analytics account and change the Code
        // TODO: Check Meta tags: description, keywords, Facebook images, canonical pages
        // TODO: Place favicon.ico and apple-touch-icon.png in the root directory
        // TODO: Check page titles
        // TODO: Validate HTML / CSS ?
        // TODO: Check webdevchecklist.com
        // TODO: Check debug mode
        // TODO: Switch off the site on the dev. server
        // TODO: Get rid of any sensitive informations from our company (websites@...) in the code
    </script>
    <script src="https://www.google-analytics.com/analytics.js" async defer></script>
</body>
</html>