﻿namespace Website.App.Localization
{


    public class Language
    {
        public Language(string isoCode)
        {
            IsoCode = isoCode;
        }

        public string DataBaseConnectionString { get; private set; }
        public string DBName { get; private set; }
        public string IsoCode { get; private set; }
        public string Name { get; private set; }

        public Language ConnectionString(string dbName, string connectionString)
        {
            DBName = dbName;
            DataBaseConnectionString = connectionString;
            return this;
        }

        public Language Named(string name)
        {
            Name = name;
            return this;
        }
    }
}