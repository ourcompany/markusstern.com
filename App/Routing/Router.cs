﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;
using System.Web.SessionState;
using Microsoft.AspNet.Identity.Owin;
using ourCompany.cms.Data;
using Website.App.Localization;
using Website.App.Routing.Routes;
using Website.Tools;

namespace Website.App.Routing
{
    /// <summary>
    /// Router class
    /// </summary>
    public class Router
    {
        public const string LANG_ROUTE_ATTRIBUTE = "Lang";
        private static List<IRoute> _Routes;
        public static Language[] Languages;
        public static Predicate<RequestContext> IsPreview { get; set; }
        public static void Register(params IRoute[] routes)
        {
            _Routes = routes.ToList();

            var hasLanguages = Languages.Count() > 1;
            var groups = _Routes.GroupBy(s => s.URL).SelectMany(group => Languages.Select(l => new { Url = (hasLanguages ? l.IsoCode + "/" : "") + group.Key, Routes = group.ToArray(), Lang = l.IsoCode }));
            foreach (var group in groups)
            {
                var route = new Route(group.Url, new RouteValueDictionary { [LANG_ROUTE_ATTRIBUTE] = group.Lang }, new RouterHandler(group.Routes));
                RouteTable.Routes.Add(route);
            }
            // handle startpage :
            if (hasLanguages)
            {
                RouteTable.Routes.Add(new Route("", new LanguageRedirectHandler()));
            }
        }

        private class LanguageRedirectHandler : IRouteHandler
        {
            public IHttpHandler GetHttpHandler(RequestContext requestContext)
            {
                return new AsyncHandler();
            }

            private class AsyncHandler : IHttpHandler
            {
                public bool IsReusable => false;

                public void ProcessRequest(HttpContext context)
                {
                    //var prefered = context.Request.UserLanguages != null ? new List<string>(context.Request.UserLanguages) : null;
                    //var result = Helpers.GetLanguageString(prefered, Configuration.Languages.Select(l => l.IsoCode).ToList());
                    //context.Response.Redirect("/" + result);
                    context.Response.Redirect("/pt");
                }
            }
        }

        public class RouterHandler : IRouteHandler
        {
            private IEnumerable<IRoute> _Routes;

            public RouterHandler(IEnumerable<IRoute> routes)
            {
                _Routes = routes;
            }

            public IHttpHandler GetHttpHandler(RequestContext requestContext)
            {

                var lang = (string)requestContext.RouteData.Values[LANG_ROUTE_ATTRIBUTE];

                var db = DB.FromLang(lang);
                //if (IsPreview != null)
                //    db.OnlyPublished = !IsPreview(requestContext);

                requestContext.HttpContext.GetOwinContext().Set(db);

                foreach (var match in _Routes)
                {
                    var result = match.Parse(requestContext);
                    if (result.Success)
                    {
                        // adds the values coming from the context
                        requestContext.RouteData.Values.ToList().ForEach(x => result.RouteValues.Add(x.Key, x.Value));

                        if (requestContext.HttpContext.Request.QueryString.AllKeys.Contains("switch-lang"))
                        {
                            var targetLang = requestContext.HttpContext.Request.QueryString["switch-lang"];
                            return new LanguageRedirectHandler(requestContext, match, result.RouteValues, targetLang);
                        }
                        return new AsyncHandler(requestContext, match, result.RouteValues);
                    }
                }
                return new NotFoundHandler();
            }

            private class AsyncHandler : IHttpAsyncHandler, IRequiresSessionState
            {
                public AsyncHandler(
                    RequestContext requestContext,
                    IRoute route,
                    IDictionary<string, object> parsedValues)
                {
                    Route = route;
                    RequestContext = requestContext;
                    RouteValues = parsedValues;
                }

                public HttpContext Context { get; private set; }

                public bool IsReusable
                {
                    get
                    {
                        return true;
                    }
                }

                public RequestContext RequestContext { get; private set; }
                public IRoute Route { get; private set; }
                public IDictionary<string, object> RouteValues { get; private set; }

                public IAsyncResult BeginProcessRequest(
                    HttpContext context,
                    AsyncCallback cb,
                    object extraData)
                {
                    Context = context;
                    return Route.ExcecuteAsync(context, RequestContext, RouteValues).ToApm(cb, extraData);
                }

                public void EndProcessRequest(IAsyncResult result)
                {
                    try
                    {
                        if (!((Task<bool>)result).Result)
                            throw new Exception("impossible to excecute the route");
                    }
                    catch (AggregateException ae) { throw ae.InnerException; }
                    catch (Exception ae) { throw ae.InnerException; }
                }

                public void ProcessRequest(HttpContext context)
                {
                    throw new NotImplementedException();
                }
            }

            private class LanguageRedirectHandler : IHttpHandler
            {
                public string Targetlang { get; set; }
                public RequestContext RequestContext { get; private set; }
                public IRoute Route { get; private set; }
                public IDictionary<string, object> RouteValues { get; private set; }

                public LanguageRedirectHandler(RequestContext requestContext, IRoute route, IDictionary<string, object> parsedValues, string targetLang)
                {
                    Route = route;
                    RequestContext = requestContext;
                    RouteValues = parsedValues;
                    Targetlang = targetLang;
                }

                public bool IsReusable => false;


                public void ProcessRequest(HttpContext context)
                {
                    try
                    {
                        var newUrl = Route.Parser.TranslateUrl(Targetlang, RouteValues);
                        context.Response.Redirect("/" + Targetlang + "/" + newUrl);
                    }
                    catch (ThreadAbortException)
                    {
                        // thread is aborted on redirect
                    }
                    catch (Exception)
                    {
                        context.Response.Redirect("/" + Targetlang);
                    }
                }
            }

            private class NotFoundHandler : IHttpHandler
            {
                public bool IsReusable => true;

                public void ProcessRequest(HttpContext context)
                {
                    context.Response.ClearHeaders();
                    context.Response.Clear();

                    context.Response.StatusCode = 404;
                    context.Response.SuppressContent = true;
                    context.Response.End();
                }
            }
        }
    }


}