﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ourCompany.cms.Model;
using ourCompany.cms.Data;

namespace Website.App.Routing
{
    public static class FromRouteAttributeExtensions
    {
        public static bool SetRouteValues<T>(this T instance, IDictionary<string, object> values)
        {
            var result = false;
            var type = instance.GetType();
            var props = type
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Select(p => new { name = p.Name, atts = p.GetCustomAttributes<FromRouteAttribute>(true), property = p })
                .Where(p => p.atts.Any());

            foreach (var prop in props)
            {
                if (!prop.property.CanWrite) continue;

                foreach (var att in prop.atts)
                {
                    if (!values.ContainsKey(att.Name ?? prop.name)) continue;
                    var value = values[att.Name ?? prop.name];

                    if (!prop.property.PropertyType.IsAssignableFrom(value.GetType()))
                    {
                        if (value is DBRef) value = ((DBRef)value).GetNode();
                        if (!prop.property.PropertyType.IsAssignableFrom(value.GetType()))
                            continue;
                    }
                    prop.property.SetValue(instance, value);
                    result = true;
                    break;
                }
            }
            return result;
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    internal class FromRouteAttribute : Attribute
    {
        public string Name { get; set; }
    }
}