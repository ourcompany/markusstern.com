﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.Identity.Owin;
using ourCompany.cms.Data;
using Website.App.Routing.Views;

namespace Website.App.Routing.Routes
{
    public interface IRoute
    {
        RouteParser Parser { get; }

        string URL { get; }

        Task<bool> ExcecuteAsync(HttpContext context, RequestContext requestContext, IDictionary<string, object> routeValues);

        RouteResult Parse(RequestContext requestContext);
    }

    public struct RouteResult
    {
        public IDictionary<string, object> RouteValues;
        public bool Success;
    }

    public static class RouteExtensions
    {
        public static Route<T> Calls<T>(this Route<T> route, Action<T, Route<T>, RequestContext, IDictionary<string, object>> action) where T : class, IRouted, IHttpHandler, new()
        {
            route.AddAction(action);
            return route;
        }

        public static Route<T> Named<T>(this Route<T> route, string name) where T : class, IRouted, IHttpHandler, new()
        {
            route.Name = name;
            return route;
        }

        public static Route<T> Redirects<T>(this Route<T> route, Func<Route<T>, T, string> getURL, bool permanent = false) where T : class, IRouted, IHttpHandler, new()
        {
            route.SetRedirect(getURL, permanent);
            return route;
        }
    }

    public abstract class Route<T> : IRoute where T : class, IRouted, IHttpHandler, new()
    {
        private readonly Collection<Action<T, Route<T>, RequestContext, IDictionary<string, object>>> _Actions = new Collection<Action<T, Route<T>, RequestContext, IDictionary<string, object>>>();

        private readonly Collection<_ParameterSelector> _ParametersSelectors = new Collection<_ParameterSelector>();

        private readonly Collection<ConditionalView> _Renders = new Collection<ConditionalView>();

        private _RedirectionArguments _Redirection;



        public Predicate<RequestContext> Condition { get; private set; }

        public Func<Route<T>, T, DateTime?> LastModification { get; set; }

        public string Name { get; set; }

        public RouteParser Parser { get; private set; }

        public string URL
        {
            get
            {
                return Parser.URL;
            }
        }

        public void AddAction(Action<T, Route<T>, RequestContext, IDictionary<string, object>> action)
        {
            _Actions.Add(action);
        }

        /// <summary>
        /// Excecutes the Action associated with the route in a specified context.
        /// </summary>
        /// <param name="env"></param>
        /// <param name="path">The path.</param>
        public abstract Task<bool> ExcecuteAsync(HttpContext context, RequestContext requestContext, IDictionary<string, object> routeValues);

        public virtual T GenerateHandler(RequestContext requestContext, IDictionary<string, object> routeValues)
        {
            return new T();
        }



        public Route<T> If(Predicate<RequestContext> condition)
        {
            Condition = condition;
            return this;
        }

        public Route<T> Matching(string pattern)
        {
            Parser = new RouteParser(this, pattern);

            return this;
        }

        public Route<T> Parameters(string key, Func<Route<T>, T, object> selector = null)
        {
            _ParametersSelectors.Add(new _ParameterSelector { Key = key, Selector = selector });
            return this;
        }

        public RouteResult Parse(RequestContext requestContext)
        {
            if (Condition != null && !Condition(requestContext))
                return new RouteResult() { Success = false };

            var values = Parser.Parse(requestContext);
            return new RouteResult() { Success = values != null, RouteValues = values };
        }

        public Route<T> Renders<ViewType>(Predicate<Route<T>> predicate = null) where ViewType : IView<T>, new()
        {
            return Renders((r, e) => new ViewType(), predicate != null ? new Func<Route<T>, T, bool>((r, e) => predicate(r)) : null);
        }

        public Route<T> Renders<ViewType>(Func<Route<T>, T, bool> predicate) where ViewType : IView<T>, new()
        {
            return Renders((r, e) => new ViewType(), predicate);
        }

        public Route<T> Renders(Func<Route<T>, T, IView<T>> evaluator, Func<Route<T>, T, bool> predicate = null)
        {
            _Renders.Add(new ConditionalView { Evaluator = evaluator, Predicate = predicate });
            return this;
        }

        public Route<T> SetLastModification(Func<Route<T>, T, DateTime?> evaluator)
        {
            LastModification = evaluator;
            return this;
        }

        public void SetRedirect(Func<Route<T>, T, string> getURL, bool permanent = false)
        {
            _Redirection = _Redirection ?? new _RedirectionArguments();

            _Redirection.Evaluator = getURL;
            _Redirection.Permanent = permanent;
        }

        protected void ExcecuteAsync(T env, HttpContext context, RequestContext requestContext, IDictionary<string, object> routeValues)
        {
            foreach (var action in _Actions)
            {
                action(env, this, requestContext, routeValues);
            }
            if (_Redirection != null)
            {
                var url = _Redirection.Evaluator(this, env);
                if (!string.IsNullOrEmpty(url))
                    if (_Redirection.Permanent)
                        requestContext.HttpContext.Response.RedirectPermanent(url);
                    else
                        requestContext.HttpContext.Response.Redirect(url);
            }
            env.SetRouteValues(routeValues);
        }

        protected async Task RenderAsync(T layout, HttpContext context, RequestContext requestContext, IDictionary<string, object> routeValues)
        {

            foreach (var render in _Renders)
            {
                if (render.Predicate == null || render.Predicate(this, layout))
                {
                    if (render.Evaluator != null)
                    {
                        var view = render.Evaluator(this, layout);
                        if (view != null)
                        {

                            view.SetRouteValues(routeValues);

                            view.Context = requestContext;
                            view.Layout = layout;
                            view.Route = this;

                            view.Database = context.GetOwinContext().Get<DB>();



                            await view.Init().ContinueWith(t =>
                            {
                                if (t.IsFaulted || !t.Result)
                                    return Task.FromResult(false);
                                return view.Render();
                            }).Unwrap();
                        }
                    }
                }
            }
        }

        private struct _ParameterSelector
        {
            public string Key;
            public Func<Route<T>, T, object> Selector;
        }

        private struct ConditionalView
        {
            public Func<Route<T>, T, IView<T>> Evaluator;
            public Func<Route<T>, T, bool> Predicate;
        }

        private class _RedirectionArguments
        {
            public Func<Route<T>, T, string> Evaluator;
            public bool Permanent;
        }
    }
}