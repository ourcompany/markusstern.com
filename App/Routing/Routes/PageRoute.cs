﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Compilation;
using System.Web.Routing;
using System.Web.UI;
using Microsoft.AspNet.Identity.Owin;
using ourCompany.cms.Data;

namespace Website.App.Routing.Routes
{
    public class PageRoute<T> : Route<T> where T : Page, IRouted, new()
    {
        public PageRoute(string virtualPath)
        {
            VirtualPath = virtualPath;
        }

        public string VirtualPath { get; private set; }

        public override Task<bool> ExcecuteAsync(HttpContext context, RequestContext requestContext, IDictionary<string, object> routeValues)
        {
            var env = BuildManager.CreateInstanceFromVirtualPath(VirtualPath, typeof(T)) as T;


            env.Database = context.GetOwinContext().Get<DB>();

            env.PreInit += (object sender, EventArgs e) =>
            {
                ExcecuteAsync(env, context, requestContext, routeValues);
            };

            env.Init += (object sender, EventArgs e) =>
            {
                //env.ExecuteRegisteredAsyncTasks();
            };

            env.PreLoad += (object sender, EventArgs e) =>
            {
                Tools.AsyncHelper.RunSync(() => RenderAsync(env, context, requestContext, routeValues));
                //env.RegisterAsyncTask(new PageAsyncTask(() => RenderAsync(env, context, requestContext, routeValues)));
                //env.ExecuteRegisteredAsyncTasks();
            };

            env.ProcessRequest(context);
            return Task.FromResult(true);
        }
    }
}