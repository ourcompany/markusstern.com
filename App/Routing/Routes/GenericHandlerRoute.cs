﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.Identity.Owin;
using ourCompany.cms.Data;
using Website.Layouts.Base;

namespace Website.App.Routing.Routes
{
    public class GenericAsyncHandlerRoute<T> : Route<T> where T : class, IRouted, IGenericHandler, new()
    {
        public async override Task<bool> ExcecuteAsync(HttpContext context, RequestContext requestContext, IDictionary<string, object> routeValues)
        {
            var env = new T();

            env.Database = context.GetOwinContext().Get<DB>();
            ExcecuteAsync(env, context, requestContext, routeValues);
            var render = RenderAsync(env, context, requestContext, routeValues);
            await render.ContinueWith(t =>
            {
                if (t.IsFaulted) throw t.Exception;
                return env.ProcessRequestAsync(context);
            }).Unwrap();

            return true;
        }
    }

    public class GenericHandlerRoute<T> : Route<T> where T : class, IRouted, IHttpHandler, new()
    {
        public async override Task<bool> ExcecuteAsync(HttpContext context, RequestContext requestContext, IDictionary<string, object> routeValues)
        {
            var env = new T();

            env.Database = context.GetOwinContext().Get<DB>();
            ExcecuteAsync(env, context, requestContext, routeValues);
            var render = RenderAsync(env, context, requestContext, routeValues);
            await render.ContinueWith(t =>
            {
                if (t.IsFaulted) throw t.Exception;
                env.ProcessRequest(context);
            });

            return true;
        }
    }
}