﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;
using ourCompany.cms.Data;
using Website.App.Routing.Routes;

namespace Website.App.Routing.Views
{
    public abstract class View<T> : IView<T> where T : class, IRouted, IHttpHandler, new()
    {
        /// <summary>
        /// Gets or sets Request context.
        /// </summary>
        /// <value>The context.</value>
        public RequestContext Context { get; set; }

        public DB Database { get; set; }

        [FromRoute(Name = Router.LANG_ROUTE_ATTRIBUTE)]
        public string Lang { get; set; }

        public T Layout { get; set; }


        public Route<T> Route { get; set; }

        public Task<bool> Init() => Task.FromResult(true);

        /// <summary>
        /// Renders the view in the specific Page
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="context">The context.</param>
        /// <param name="route">The route.</param>
        /// <returns></returns>
        public abstract Task<bool> Render();
    }
}