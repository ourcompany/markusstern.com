﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.Identity.Owin;
using MongoDB.Bson;
using MongoDB.Driver;
using ourCompany.cms.Data;
using Website.App.Routing.Routes;
using Website.Tools;

namespace Website.App.Routing
{
    [Serializable]
    public class RouteParser
    {
        private readonly Regex _TokenRegex = new Regex(@"(?<token>{[^}]+}|[^/]+)(?:/)?");

        public RouteParser(IRoute route, string pattern)
        {
            Route = route;
            RoutePattern = pattern;
            ParseRouteFormat();
        }

        public interface IRouteToken
        {
            string Name { get; set; }
        }

        public IRoute Route
        {
            get; private set;
        }

        /// <summary>
        /// This is the route template that values are extracted based on.
        /// </summary>
        /// <value>
        /// A string containing variables denoted by the <c>VariableStartChar</c> and the <c>VariableEndChar</c>
        /// </value>
        public string RoutePattern { get; set; }

        /// <summary>
        /// A hash set of all variable names parsed from the <c>RouteFormat</c>.
        /// </summary>
        public IDictionary<string, IRouteToken> Tokens { get; set; }

        public string URL
        {
            get; private set;
        }

        /// <summary>
        /// Extract variable values from a given instance of the route you're trying to parse.
        /// </summary>
        /// <param name="env"></param>
        /// <param name="routeInstance">The route instance.</param>
        /// <returns>A dictionary of Variable names mapped to values.</returns>
        public IDictionary<string, object> Parse(RequestContext context)
        {
            //context.RouteData.Values[];
            //var matchCollection = RouteRegex.Match(routeInstance);
            //if (!matchCollection.Success)
            //    return false;

            var result = new Dictionary<string, object>();
            var mongoDatabase = context.HttpContext.GetOwinContext().Get<DB>();
            //var parameters = new List<KeyValuePair<string, object>>();
            //var matches = new List<KeyValuePair<string, string>>();
            foreach (var contextValue in context.RouteData.Values)
            {
                if (!Tokens.ContainsKey(contextValue.Key))
                    continue;
                var token = Tokens[contextValue.Key];

                var value = contextValue.Value;
                try
                {
                    TypeSwitch
                        .On(token)
                        .Case<_RouteMongoToken>((t) =>
                        {
                            var dbref = mongoDatabase.Find(t.NodeName, Builders<BsonDocument>.Filter.Eq(t.Attribute, value)).FirstOrDefault();
                            if (dbref == null) throw new Exception();
                            result.Add(token.Name, dbref);
                        })
                        .Case<_RouteVariableToken>((t) =>
                        {
                            result.Add(token.Name, value);
                        })
                        .Case<_RouteNamedConstantToken>((t) =>
                        {
                            result.Add(token.Name, t.Constant);
                        });
                }
                catch
                {
                    return null;
                }
            }
            return result;
        }

        /// <summary>
        /// Initialize the Variables set based on the <c>RouteFormat</c>
        /// </summary>
        public void ParseRouteFormat()
        {
            var routeMongoRegex = new Regex(_RouteMongoToken.Pattern, RegexOptions.None);
            var routeVariableRegex = new Regex(_RouteVariableToken.Pattern, RegexOptions.None);
            var routeConstantRegex = new Regex(_RouteConstantToken.Pattern, RegexOptions.None);
            var routeNamedConstantRegex = new Regex(_RouteNamedConstantToken.Pattern, RegexOptions.None);

            var routeRegexParts = new List<string>();
            Tokens = new Dictionary<string, IRouteToken>();
            var url = new List<string>();
            var counter = 0;
            if (RoutePattern.Check())
                foreach (Match tokenMatch in _TokenRegex.Matches(this.RoutePattern))
                {
                    var part = tokenMatch.Groups["token"].Value;
                    Match match;
                    if ((match = routeMongoRegex.Match(part)).Success)
                    {
                        var tokenid = "token" + (counter++);
                        var token = new _RouteMongoToken { Name = match.Groups["name"].Value, Attribute = match.Groups["attribute"].Value, NodeName = match.Groups["nodename"].Value };

                        Tokens.Add(tokenid, token);
                        url.Add(string.Format("{{{0}}}", tokenid));
                        //routeRegexParts.Add("(?<{0}>[^/$]+)".With(token.Name));
                    }
                    else if ((match = routeVariableRegex.Match(part)).Success)
                    {
                        var tokenid = "token" + (counter++);
                        var token = new _RouteVariableToken { Name = match.Groups["name"].Value };
                        Tokens.Add(tokenid, token);
                        url.Add(string.Format("{{{0}}}", tokenid));
                        //routeRegexParts.Add("(?<{0}>[^/$]+)".With(token.Name));
                    }
                    else if ((match = routeNamedConstantRegex.Match(part)).Success)
                    {
                        var tokenid = "token" + (counter++);
                        var token = new _RouteNamedConstantToken { Name = match.Groups["name"].Value, Constant = match.Groups["constant"].Value };
                        Tokens.Add(tokenid, token);
                        url.Add(string.Format("{{{0}}}", tokenid));
                        //routeRegexParts.Add("(?<{0}>".With(token.Name) + Regex.Escape(token.Constant) + ")");
                    }
                    else if ((match = routeConstantRegex.Match(part)).Success)
                    {
                        var tokenid = "token" + (counter++);
                        var token = new _RouteConstantToken { Name = match.Groups["name"].Value };
                        Tokens.Add(tokenid, token);
                        url.Add(token.Name);
                        //routeRegexParts.Add(Regex.Escape(part));
                    }
                }
            URL = string.Join("/", url);
            //RouteRegex = new Regex("^" + String.Join("/", routeRegexParts) + "(\\?|$)", RegexOptions.Compiled);
        }

        //public Regex RouteRegex { get; set; }
        private class _RouteConstantToken : IRouteToken
        {
            public const string Pattern = @"(?<name>[^{}\:]+)";
            public string Name { get; set; }
        }

        private class _RouteMongoToken : IRouteToken
        {
            public const string Pattern = @"{(?<name>[^}\:]+):(?<nodename>[^}\:]+):(?<attribute>[^}\:]+)}";

            public string Attribute { get; set; }
            public string Name { get; set; }
            public string NodeName { get; set; }
        }

        private class _RouteNamedConstantToken : IRouteToken
        {
            public const string Pattern = @"{(?<name>[^}\:]+):(?<constant>[^}\:@\/]+)}";
            public string Constant { get; set; }
            public string Name { get; set; }
        }

        private class _RouteVariableToken : IRouteToken
        {
            public const string Pattern = @"{(?<name>[^}\:]+)}";
            public string Name { get; set; }
        }

        public string TranslateUrl(string newlang, IDictionary<string, object> routeValues)
        {

            var targetDatabase = DB.FromLang(newlang);

            var result = new List<string>();

            foreach (var tokenKey in Tokens.Keys)
            {
                var token = Tokens[tokenKey];
                TypeSwitch.OnType(token.GetType())
                    .Case<_RouteMongoToken>(() =>
                    {
                        var mongoToken = (_RouteMongoToken)token;
                        var dbref = routeValues[mongoToken.Name] as DBRef;

                        var targetNode = targetDatabase.FollowRef<BsonDocument>(dbref.Ref);
                        //if (!targetNode.GetValue(Node.PUBLISHED_ATTRIBUTE, true).AsBoolean)
                        //    throw new Exception("node not published");
                        //if (!targetNode.GetValue("Online", true).AsBoolean)
                        //    throw new Exception("node not published");
                        result.Add(targetNode.GetValue(mongoToken.Attribute, "").AsString);
                    })
                    .Case<_RouteVariableToken>(() =>
                    {
                        result.Add(routeValues[tokenKey] as string);
                    })
                    .Case<_RouteNamedConstantToken>(() =>
                    {
                        result.Add(((_RouteNamedConstantToken)token).Constant);
                    })
                    .Case<_RouteConstantToken>(() => result.Add(token.Name));
            }
            return string.Join("/", result);
        }
    }
}