﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using ourCompany.cms.Data;

namespace Website.Tools
{
    /// <summary>
    /// Summary description for ImageSize
    /// </summary>
    public class ImageSize
    {
        private const string _COLLECTION_NAME = "Website.Tools.ImageSize.SizeInfo";
        private const string _ERROR_MESSAGE = "Could not recognise image format.";

        private static readonly Dictionary<byte[], Func<BinaryReader, Size>> _ImageFormatDecoders = new Dictionary<byte[], Func<BinaryReader, Size>>
                                                                                                    {
            { new byte[]{ 0x42, 0x4D }, _DecodeBitmap},
            { new byte[]{ 0x47, 0x49, 0x46, 0x38, 0x37, 0x61 }, _DecodeGif },
            { new byte[]{ 0x47, 0x49, 0x46, 0x38, 0x39, 0x61 }, _DecodeGif },
            { new byte[]{ 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A }, _DecodePng },
            { new byte[]{ 0xff, 0xd8 }, _DecodeJfif },
        };

        /// <summary>
        /// Prevents a default instance of the <see cref="ImageSize"/> class from being created.
        /// </summary>
        private ImageSize()
        {
        }

        public enum ProcessingMethod
        {
            Header,
            Bitmap
        }

        private static MongoContext _Context => MongoContext.CreateFromConfig();

        private static IMongoCollection<SizeInfo> _Sizes => _Context.DefaultDatabase.GetCollection<SizeInfo>(_COLLECTION_NAME);

        public static async Task Flush()
        {
            await _Context.DefaultDatabase.Database.DropCollectionAsync(_COLLECTION_NAME);
        }

        /// <summary>
        /// Gets the dimensions of an image.
        /// </summary>
        /// <param name="path">The path of the image to get the dimensions of.</param>
        /// <returns>The dimensions of the specified image.</returns>
        /// <exception cref="ArgumentException">The image was of an unrecognised format.</exception>
        public static Size GetDimensions(string path)
        {
            using (var binaryReader = new BinaryReader(File.OpenRead(path)))
            {
                try
                {
                    return GetDimensions(binaryReader);
                }
                catch (ArgumentException e)
                {
                    if (e.Message.StartsWith(_ERROR_MESSAGE))
                    {
                        throw new ArgumentException(_ERROR_MESSAGE, "path", e);
                    }
                    throw;
                }
            }
        }

        /// <summary>
        /// Gets the dimensions of an image.
        /// </summary>
        /// <param name="binaryReader">The reader of the image to get the dimensions of.</param>
        /// <returns>The dimensions of the specified image.</returns>
        /// <exception cref="ArgumentException">The image was of an unrecognised format.</exception>
        public static Size GetDimensions(BinaryReader binaryReader)
        {
            var maxMagicBytesLength = _ImageFormatDecoders.Keys.OrderByDescending(x => x.Length).First().Length;

            var magicBytes = new byte[maxMagicBytesLength];

            for (var i = 0; i < maxMagicBytesLength; i += 1)
            {
                magicBytes[i] = binaryReader.ReadByte();

                foreach (var kvPair in _ImageFormatDecoders.Where(kvPair => _StartsWith(magicBytes, kvPair.Key)))
                {
                    return kvPair.Value(binaryReader);
                }
            }

            throw new ArgumentException(_ERROR_MESSAGE, "binaryReader");
        }

        public static async Task<Size> SizeOf(string localPath, int maxHeight = 0, int maxWidth = 0)
        {
            int fullHeight, fullWidth;
            var filter = Builders<SizeInfo>.Filter.Eq(s => s.LocalPath, localPath);

            var size = (await _Sizes.FindAsync(filter)).FirstOrDefault();
            if (size == null)
            {
                // try to get the image
                ProcessingMethod processingMethod;
                try
                {
                    var s = GetDimensions(localPath);
                    fullHeight = s.Height;
                    fullWidth = s.Width;
                    processingMethod = ProcessingMethod.Header;
                }
                catch (Exception)
                {
                    try
                    {
                        using (var b = Image.FromFile(localPath))
                        {
                            fullHeight = b.Height;
                            fullWidth = b.Width;
                            processingMethod = ProcessingMethod.Bitmap;
                        }
                    }
                    catch (Exception)
                    {
                        return new Size();
                    }
                }
                size = new SizeInfo { LocalPath = localPath, Size = new Size(fullWidth, fullHeight), ProcessingMethod = processingMethod };
                await _Sizes.InsertOneAsync(size);
            }
            else
            {
                fullHeight = size.Size.Height;
                fullWidth = size.Size.Width;
            }

            maxWidth = maxWidth > 0 ? maxWidth : fullWidth;
            maxHeight = maxHeight > 0 ? maxHeight : fullHeight;

            var
                factor = Math.Min(maxWidth / fullWidth, maxHeight / fullHeight);

            return new Size(fullWidth * factor, fullHeight * factor);
        }

        private static Size _DecodeBitmap(BinaryReader binaryReader)
        {
            binaryReader.ReadBytes(16);
            int width = binaryReader.ReadInt32(),
                height = binaryReader.ReadInt32();
            return new Size(width, height);
        }

        private static Size _DecodeGif(BinaryReader binaryReader)
        {
            int width = binaryReader.ReadInt16(),
                height = binaryReader.ReadInt16();
            return new Size(width, height);
        }

        private static Size _DecodeJfif(BinaryReader binaryReader)
        {
            while (binaryReader.ReadByte() == 0xff)
            {
                var marker = binaryReader.ReadByte();
                var chunkLength = _ReadLittleEndianInt16(binaryReader);

                if (marker == 0xc0)
                {
                    binaryReader.ReadByte();

                    int height = _ReadLittleEndianInt16(binaryReader);
                    int width = _ReadLittleEndianInt16(binaryReader);
                    return new Size(width, height);
                }

                binaryReader.ReadBytes(chunkLength - 2);
            }

            throw new ArgumentException(_ERROR_MESSAGE);
        }

        private static Size _DecodePng(BinaryReader binaryReader)
        {
            binaryReader.ReadBytes(8);
            int width = _ReadLittleEndianInt32(binaryReader),
                height = _ReadLittleEndianInt32(binaryReader);
            return new Size(width, height);
        }

        private static short _ReadLittleEndianInt16(BinaryReader binaryReader)
        {
            var bytes = new byte[sizeof(short)];
            for (var i = 0; i < sizeof(short); i += 1)
            {
                bytes[sizeof(short) - 1 - i] = binaryReader.ReadByte();
            }
            return BitConverter.ToInt16(bytes, 0);
        }

        private static int _ReadLittleEndianInt32(BinaryReader binaryReader)
        {
            var bytes = new byte[sizeof(int)];
            for (int i = 0; i < sizeof(int); i += 1)
            {
                bytes[sizeof(int) - 1 - i] = binaryReader.ReadByte();
            }
            return BitConverter.ToInt32(bytes, 0);
        }

        private static bool _StartsWith(byte[] thisBytes, byte[] thatBytes)
        {
            for (var i = 0; i < thatBytes.Length; i += 1)
            {
                if (thisBytes[i] != thatBytes[i])
                {
                    return false;
                }
            }
            return true;
        }

        public class SizeInfo
        {
            public string LocalPath;
            public ProcessingMethod ProcessingMethod;
            public Size Size;
        }
    }
}