@echo off 
set /p BranchPath=<../git/HEAD
set BranchPath=%BranchPath:ref: =%
set BranchName=%BranchPath:refs/heads/=%
set /p Rev=<../.git/%BranchPath%

(
echo Branch: %BranchName%
echo Commit Hash: %Rev%
echo Time: %DATE% %TIME%
)>../Revision.txt
pause