/// <reference path="typings/requirejs/require.d.ts" />
var Polyfills = ["Lib/modernizr-3.3.1"];
if (!("classList" in document.documentElement))
    Polyfills.push("Polyfills/classList");
if (!window.addEventListener)
    Polyfills.push("Polyfills/addEventListener");
if (!document.getElementsByClassName)
    Polyfills.push("Polyfills/getElementsByClassName");
if (!window.matchMedia)
    Polyfills.push("Polyfills/matchMedia", "Polyfills/matchMedia.addListener");
if (document.getElementsByTagName("html")[0].className.indexOf("lt-ie9") > -1)
    Polyfills.push("Polyfills/Array.slice");
if (!(window.requestAnimationFrame && window.cancelAnimationFrame))
    Polyfills.push("Polyfills/requestAnimationFrame");
if (!("startsWith" in String.prototype))
    Polyfills.push("Polyfills/String.startsWith");
requirejs(Polyfills, function () {
    if (!Modernizr.objectfit) {
        requirejs(["Polyfills/object-fit-images"], function () {
            objectFitImages();
        });
    }
    var required = document.body.getAttribute("data-require");
    if (!required || !required.length)
        return;
    requirejs(required.split(','), function () { });
});
