/**
 * Enum for Directions
 * @enum {number} - flag values
 */
var Direction;
(function (Direction) {
    Direction[Direction["Center"] = 0] = "Center";
    Direction[Direction["Top"] = 1] = "Top";
    Direction[Direction["Bottom"] = 2] = "Bottom";
    Direction[Direction["Right"] = 4] = "Right";
    Direction[Direction["Left"] = 8] = "Left";
    Direction[Direction["Over"] = 16] = "Over";
    Direction[Direction["Under"] = 32] = "Under";
})(Direction || (Direction = {}));
/**
 * Enum for keys keyboard charcode
 * @enum {number}
 */
var KeyCharcode;
(function (KeyCharcode) {
    KeyCharcode[KeyCharcode["Enter"] = 13] = "Enter";
    KeyCharcode[KeyCharcode["Escape"] = 27] = "Escape";
    KeyCharcode[KeyCharcode["Space"] = 32] = "Space";
    KeyCharcode[KeyCharcode["Left"] = 37] = "Left";
    KeyCharcode[KeyCharcode["Up"] = 38] = "Up";
    KeyCharcode[KeyCharcode["Right"] = 39] = "Right";
    KeyCharcode[KeyCharcode["Down"] = 40] = "Down";
})(KeyCharcode || (KeyCharcode = {}));
