﻿//interface GoogleAnalyticsTracker {
//}

//interface GoogleAnalytics {
//    new (action: string, ...args: Array<any>): GoogleAnalyticsTracker;
//    new (fct: (tracker:GoogleAnalyticsTracker) => void): GoogleAnalyticsTracker;
//    new (action: "send", args: "pageviews"): GoogleAnalyticsTracker;
//    /*
//     * Creates a new default tracker object.
//     * @param {String} trackingId The web property ID for the site being tracked.
//     * @param {String} cookieDomain
//     * @param {String} cookieName
//     * @param {Object} opt_configObject An optional object containing configuration field/value pairs.
//     */
//    new (action: "create", trackingId?: string, cookieDomain?: string, cookieName?: string, opt_configObject?: Object): GoogleAnalyticsTracker;

//    /*
//     * Returns the tracker object with the given name, or null if no tracker object with the given name currently exists.
//     * @param {String} name the given name of the tracker object
//     * @returns {GoogleAnalyticsTracker} the tracker
//     */
//    getByName(name: string): GoogleAnalyticsTracker;

//    /*
//     * Returns the tracker object with the given name, or null if no tracker object with the given name currently exists.
//     * @param {String} name the given name of the tracker object
//     * @returns {GoogleAnalyticsTracker} the tracker
//     */
//    getAll(): Array<GoogleAnalyticsTracker>;

//    /*
//     * Sends a tracking beacon to Google’s collection servers. The optional field object allows users to override one or more field values for this hit only.
//     * @param {String} trackingId The web property ID for the site being tracked.
//     * @param {GoogleAnalyticsHittype} hitType
//     * @param {Object} opt_configObject An optional object containing configuration field/value pairs.
//     */
//    new (action: "send", hitType: "event", opt_configObject?: Object): GoogleAnalyticsTracker;
//    new (action: "send", hitType: "social", opt_configObject?: Object): GoogleAnalyticsTracker;
//    new (action: "send", hitType: "timing", opt_configObject?: Object): GoogleAnalyticsTracker;
//    new (action: "send", hitType: "pageview", opt_configObject?: Object): GoogleAnalyticsTracker;

//}

declare var ga: any;