define(["require", "exports", "Helpers/Helpers"], function (require, exports, Helpers) {
    "use strict";
    exports.Href = Helpers.NormalizeHref((window.history.location || document.location).href);
    exports.ChangeState = function (url, title, state) {
        var href = Helpers.NormalizeHref(url);
        if (exports.Href == href)
            return; // we are there already;
        exports.Href = href;
        document.title = Helpers.DecodeHTML(title);
        if (ga)
            ga(function (tracker) {
                tracker.send('pageview', {
                    'page': href,
                    'title': title
                });
            });
        history.pushState(state, null, href);
    };
    exports.OnPopState = function (e) {
        var url = (window.history.location || document.location).href, href = Helpers.NormalizeHref(url);
        if (exports.Href != href) {
            exports.Href = href;
            exports.NavigateTo(href);
        }
    };
    exports.NavigateTo = function (url) {
        var href = Helpers.NormalizeHref(url);
        switch (href) {
            default:
                // go to href
                break;
        }
    };
});
