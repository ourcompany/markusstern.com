define(["require", "exports", "Helpers/ResponsiveImages"], function (require, exports, ResponsiveImages) {
    "use strict";
    // ---------------------------------------------------------------------------------------------------
    // DOM Ready
    // ---------------------------------------------------------------------------------------------------
    // Website.DomReady();
    // add pop stats HistoryManager to window
    // ---------------------------------------------------------------------------------------------------
    // Init
    // ---------------------------------------------------------------------------------------------------
    //enquire
    //    .register("screen and (max-width: 600px)", {
    //        match: () => {
    //            Website.Platform = Website.Platforms.Mobile;
    //            Events.Publish("ResponsiveChange");
    //        }
    //    })
    //    .register("screen and (min-width: 601px) and (max-width: 1024px)", {
    //        match: () => {
    //            Website.Platform = Website.Platforms.Tablet;
    //            Events.Publish("ResponsiveChange");
    //        }
    //    })
    //    .register("screen and (min-width: 1025px) and (max-width: 1180px)", {
    //        match: () => {
    //            Website.Platform = Website.Platforms.Laptop;
    //            Events.Publish("ResponsiveChange");
    //        }
    //    })
    //    .register("screen and (min-width: 1181px)", {
    //        match: () => {
    //            Website.Platform = Website.Platforms.Desktop;
    //            Events.Publish("ResponsiveChange");
    //        }
    //    }, true);
    // ---------------------------------------------------------------------------------------------------
    // Before Load
    // ---------------------------------------------------------------------------------------------------
    // Website.BeforeLoad();
    ResponsiveImages.Check();
});
