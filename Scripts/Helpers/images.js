define(["require", "exports"], function (require, exports) {
    "use strict";
    var _SvgExtension = /.*\.svg$/, _SubstituteSVG = function () {
        {
            var imgs = document.getElementsByTagName('img');
            var l = imgs.length;
            while (l--) {
                var img = imgs[l];
                if (img.src.match(_SvgExtension)) {
                    var noscript = document.createElement("noscript");
                    noscript.setAttribute("data-slimmage", "data-slimmage");
                    noscript.setAttribute("data-img-class", img.className);
                    noscript.setAttribute("data-img-src", img.src + '.png?max-width=1000');
                    img.parentElement.replaceChild(noscript, img);
                }
            }
        }
    };
    exports.Check = function () {
        if (!Modernizr.svg)
            _SubstituteSVG();
    };
    exports.Check();
    window.addEventListener("load", exports.Check);
});
