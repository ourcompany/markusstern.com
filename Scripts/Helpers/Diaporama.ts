﻿import Animator = require("Helpers/Animator");
import LinearEase = require("Helpers/Easing/Linear");

interface IDiaporamaOptions {
    delay?: number;// = 6000;
    duration?: number; //  = 1000
    autostart?: boolean;// = false, , easings: { show: Easing.IEasing; hide: Easing.IEasing } =
    css?: { show: string; hide: string };
    easings?: { show: IEasing; hide: IEasing };
    on?: { show?: (index: number, slide: HTMLElement) => void; hide?: (index: number, slide: HTMLElement) => void; beforeshow?: (index: number, slide: HTMLElement) => void };
}

class Diaporama {
    public Current: number = -1;

    private _Frame: number = 0;

    private _RequestAnimationFrameID: number;
    private _Slides: Array<HTMLElement>;
    private _Delay: number; // in ms

    private _Duration: number; // in frame
    private _Css: { show: string; hide: string };
    private _Easings: { show: IEasing; hide: IEasing };
    private _On: { show?: (index: number, slide: HTMLElement) => void; hide?: (index: number, slide: HTMLElement) => void; beforeshow?: (index: number, slide: HTMLElement) => void };

    constructor(slides: Array<HTMLElement>, options?: IDiaporamaOptions) {
        if (!slides.length)
            return;

        this._Slides = slides;
        this._Delay = options.delay || 6000;
        this._Duration = options.duration || 120;
        this._Easings = options.easings || { show: LinearEase, hide: LinearEase };
        this._Css = options.css;
        this._On = options.on;

        if (options.css && options.css.show) {
            var l = slides.length;
            while (l--) {
                if (slides[l].classList.contains(options.css.show))
                    this.Current = l;
                break;
            }
        }
        if (options.autostart)
            this.Play();
        else if (this.Current < 0) {
            this.Current = -1;
            this._Next();
        }
    }

    private _Playing: boolean;
    private _PlayingInte: number;

    public Play(): void {
        this._Playing = true;
        // add interval if not animating nor already existing.
        this._Next();
    }
    public Pause(): void {
        this._Playing = false;

        // remove interval already existing.
    }

    public Go(slideNum: number): void {
        console.log(slideNum);
        this.Pause();
        this._Go(slideNum);
    }

    public Next(): void {
        this.Pause();
        this._Next();
    }
    public Previous(): void {
        this.Pause();
        this._Previous();
    }

    private _Go(slideNum: number): void {
        var
            oldElt = this._Slides[this.Current],
            oldNum = this.Current,
            newElt = this._Slides[slideNum];

        this.Current = slideNum;
        this._Animate(oldElt, oldNum, newElt, this.Current);
    }

    private _Animator: { [id: number]: Animator.Engine } = {};

    private _Animate(oldElt: HTMLElement, oldNum: number, newElt: HTMLElement, newNum: number) {
        if (oldElt) {
            var oldFrom: number = oldElt.style.opacity ? parseFloat(oldElt.style.opacity) : 1;
            if (this._Animator[oldNum]) {
                this._Animator[oldNum].Cancel();
                delete this._Animator[oldNum];
            }
            if (oldFrom !== 0 && this._Easings.hide) {
                this._Animator[oldNum] = new Animator.Engine(oldFrom, 0, this._Duration, val => this._OnFrame(oldNum, val), animator => this._OnEnd(oldNum, animator, 0), this._Easings.hide);

                this._Animator[oldNum].Start();
            } else
                this._OnSlideHide(oldNum, oldElt);

            this._Hide(oldElt, oldNum);
        }
        if (newElt) {
            var newFrom: number = newElt.style.opacity ? parseFloat(newElt.style.opacity) : 0;
            if (this._Animator[newNum]) {
                this._Animator[newNum].Cancel();
                delete this._Animator[newNum];
            }
            if (this._On && this._On.show) {
                this._On.beforeshow(newNum, newElt);
            }

            if (newFrom !== 1 && this._Easings.show) {
                this._Animator[newNum] = new Animator.Engine(newFrom, 1, this._Duration, val => this._OnFrame(newNum, val), animator => this._OnEnd(newNum, animator, 1), this._Easings.show);
                this._Animator[newNum].Start();
            } else
                this._OnSlideShow(newNum, newElt);

            this._Show(newElt, newNum);
        }
    }

    private _OnFrame(index: number, val: number) {
        this._Slides[index].style.opacity = val.toString();
    }

    private _OnEnd(index: number, animator: Animator.Engine, val: number) {
        var slide = this._Slides[index];

        slide.style.opacity = val.toString();

        this._Animator[index].Cancel();
        delete this._Animator[index];

        if (val === 1) {
            this._OnSlideShow(index, slide);
        }
        else if (val === 0) {
            this._OnSlideHide(index, slide);
        }
    }

    private _OnSlideShow(index: number, slide: HTMLElement) {
        if (this._Playing && this._Slides.length > 1) {
            clearTimeout(this._PlayingInte);
            this._PlayingInte = setTimeout(this._Next.bind(this), this._Delay);
        }
        if (this._On && this._On.show) {
            this._On.show(index, slide);
        }
    }
    private _OnSlideHide(index: number, slide: HTMLElement) {
        if (this._On && this._On.hide) {
            this._On.hide(index, slide);
        }
    }

    private _Next(): void {
        this._Go((this.Current + 1) % this._Slides.length);
    }

    private _Previous(): void {
        this._Go((this.Current - 1 + this._Slides.length) % this._Slides.length);
    }

    private _Show(slideElt, slideNum) {
        if (this._Css && this._Css.show)
            slideElt.classList.add(this._Css.show);
        if (this._Css && this._Css.hide)
            slideElt.classList.remove(this._Css.hide);
    }

    private _Hide(slideElt, slideNum) {
        if (this._Css && this._Css.show)
            slideElt.classList.remove(this._Css.show);
        if (this._Css && this._Css.hide)
            slideElt.classList.add(this._Css.hide);
    }

    public Destroy() {
        if (this._PlayingInte)
            clearTimeout(this._PlayingInte);

        for (var i in this._Animator) {
            if (this._Animator.hasOwnProperty(i)) {
                this._Animator[i].Cancel();
            }
        }
        this._Animator = null;
    }
}

export = Diaporama;