﻿declare var attachEvent: any;
export var OnDomReady: boolean = true;
export var OnResize: boolean = true;
export var Verbose: boolean = false;
export var TryWebP: boolean = false;

var _WebP: boolean = false;

var _Log = (...args) => {
    if (Verbose && window.console && window.console.log)
        try { window.console.log.apply(window.console, args); }
        catch (e) { }
}

var __testingWebP;
var _BeginWebPTest = () => {
    if (!TryWebP || __testingWebP) return;
    __testingWebP = true;

    var WebP = new Image();
    WebP.onload = WebP.onerror = () => {
        _WebP = (WebP.height === 2);
    };
    WebP.src = 'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';
};

var _GetCssValue = (target, hyphenProp) => {
    var val = typeof (window.getComputedStyle) != "undefined" && window.getComputedStyle(target, null).getPropertyValue(hyphenProp);
    if (!val && target.currentStyle) {
        val = target.currentStyle[hyphenProp.replace(/([a-z])\-([a-z])/, function (a, b, c) { return b + c.toUpperCase(); })] || target.currentStyle[hyphenProp];
    }
    return val !== "none" ? val : null;
}

var _NodesToArray = function (nodeList) {
    var array = [];
    // iterate backwards ensuring that length is an UInt32
    for (var i = nodeList.length >>> 0; i--;) {
        array[i] = nodeList[i];
    }
    return array;
};

var
    _WidthRegex = /width=\d+/i,
    _HeightRegex = /height=\d+/i;

//Expects virtual, not device pixel width
var _AdjustImageSrcWithWidth = (img, originalSrc, width) => {
    var data = {
        webp: _WebP,
        width: width,
        dpr: window.devicePixelRatio || 1,
        requestedWidth: 0,
        quality: 0,
    }
    data.requestedWidth = Math.min(2048, data.width * data.dpr), //Limit size to 2048.
        data.quality = (data.dpr > 1.49) ? 80 : 90 //Default quality
    if (_WebP) data.quality = data.dpr > 1.49 ? 65 : 78;

    //Minimize variants for caching improvements; round up to nearest multiple of 160
    data.requestedWidth = data.requestedWidth - (data.requestedWidth % 160) + 160; //Will limit to 13 variations

    var oldpixels = img.getAttribute("data-pixel-width") | 0;

    if (data.requestedWidth > oldpixels) {
        //Never request a smaller image once the larger one has already started loading
        var newSrc = originalSrc.replace(_WidthRegex, "width=" + data.requestedWidth).replace(/quality=[0-9]+/i, "quality=" + data.quality);
        if (_WebP) newSrc = newSrc.replace(/format=[a-z]+/i, "format=webp");
        img.src = newSrc;
        img.setAttribute("data-pixel-width", data.requestedWidth);
        _Log("Slimming: updating " + newSrc)
    }
}

var _AdjustImageSrcWithHeight = function (img, originalSrc, height) {
    var data = {
        webp: _WebP,
        height: height,
        dpr: window.devicePixelRatio || 1,
        requestedHeight: 0,
        quality: 0,
    }
    data.requestedHeight = Math.min(2048, data.height * data.dpr), //Limit size to 2048.
        data.quality = (data.dpr > 1.49) ? 80 : 90 //Default quality
    if (_WebP) data.quality = data.dpr > 1.49 ? 65 : 78;

    //Minimize variants for caching improvements; round up to nearest multiple of 160
    data.requestedHeight = data.requestedHeight - (data.requestedHeight % 160) + 160; //Will limit to 13 variations

    var oldpixels = img.getAttribute("data-pixel-height") | 0;

    if (data.requestedHeight > oldpixels) {
        //Never request a smaller image once the larger one has already started loading
        var newSrc = originalSrc.replace(_HeightRegex, "height=" + data.requestedHeight).replace(/quality=[0-9]+/i, "quality=" + data.quality);
        if (_WebP) newSrc = newSrc.replace(/format=[a-z]+/i, "format=webp");
        img.src = newSrc;
        img.setAttribute("data-pixel-height", data.requestedHeight);
        _Log("Slimming: updating " + newSrc)
    }
}

var _AdjustImageSrcWithWidthAndHeight = function (img, originalSrc, width, height) {
    var data = {
        webp: _WebP,
        width: width,
        height: height,
        dpr: window.devicePixelRatio || 1,
        requestedWidth: 0,
        requestedHeight: 0,
        quality: 0,
    }
    data.requestedWidth = Math.min(2048, data.width * data.dpr), //Limit size to 2048.
        data.requestedHeight = Math.min(2048, data.height * data.dpr), //Limit size to 2048.

        data.quality = (data.dpr > 1.49) ? 80 : 90 //Default quality
    if (_WebP) data.quality = data.dpr > 1.49 ? 65 : 78;

    //Minimize variants for caching improvements; round up to nearest multiple of 160
    data.requestedWidth = data.requestedWidth - (data.requestedWidth % 160) + 160; //Will limit to 13 variations
    data.requestedHeight = data.requestedHeight - (data.requestedHeight % 160) + 160; //Will limit to 13 variations

    var
        oldwidth = img.getAttribute("data-pixel-width") | 0,
        oldheight = img.getAttribute("data-pixel-height") | 0;

    if (data.requestedWidth > oldwidth || data.requestedHeight > oldheight) {
        //Never request a smaller image once the larger one has already started loading
        var newSrc = originalSrc
            .replace(_WidthRegex, "width=" + data.requestedWidth)
            .replace(_HeightRegex, "height=" + data.requestedHeight)
            .replace(/quality=[0-9]+/i, "quality=" + data.quality);

        if (_WebP) newSrc = newSrc.replace(/format=[a-z]+/i, "format=webp");

        img.src = newSrc;
        img.setAttribute("data-pixel-width", data.requestedWidth);
        img.setAttribute("data-pixel-height", data.requestedHeight);

        _Log("Slimming: updating " + newSrc)
    }
}
var _GetCssPixels = (target, prop: string): number => {
    if (target.getAttribute("data-size-class")) {
        target = document.getElementsByClassName(target.getAttribute("data-size-class"))[0];
        console.log(target);
    }

    var val = _GetCssValue(target, "max-" + prop) || _GetCssValue(target, prop) || _GetCssValue(target, "min-" + prop);

    //We can return pixels directly, but not other units
    if (val.slice(-2) == "px") return parseFloat(val.slice(0, -2));

    //Create a temporary sibling div to resolve units into pixels.
    var temp = document.createElement("div");
    temp.style.overflow = temp.style.visibility = "hidden";
    target.parentNode.appendChild(temp);
    temp.style[prop] = val;
    var pixels = temp["offset" + prop[0].toUpperCase() + prop.substr(1)];
    target.parentNode.removeChild(temp);
    return pixels;
}

var _AdjustImageSrc = (img, originalSrc) => {
    var
        w = _WidthRegex.test(originalSrc),
        h = _HeightRegex.test(originalSrc);

    if (w && h)
        _AdjustImageSrcWithWidthAndHeight(img, originalSrc, _GetCssPixels(img, "width"), _GetCssPixels(img, "height"));
    else if (w)
        _AdjustImageSrcWithWidth(img, originalSrc, _GetCssPixels(img, "width"));
    else if (h)
        _AdjustImageSrcWithHeight(img, originalSrc, _GetCssPixels(img, "height"));
};

var __timeoutid = 0;

export var Prepare = (elt: HTMLElement) => {
    var n = _NodesToArray(elt.getElementsByTagName("noscript"));
    for (var i = 0, il = n.length; i < il; i++) {
        var ns = n[i];
        if (ns.getAttribute("data-img-src") !== null && ns.getAttribute("data-slimmage") === null) {
            ns.setAttribute("data-slimmage", "true");
        }
    }
}
export var Replace = (elt: HTMLElement): number => {
    var newImages = 0;
    //1. Copy images out of noscript tags, but hide 'src' attribute as data-src
    var n = _NodesToArray(elt.getElementsByTagName("noscript"));
    for (var i = 0, il = n.length; i < il; i++) {
        var ns = n[i];
        if (ns.getAttribute("data-slimmage") !== null) {
            var div = document.createElement('div');
            var contents = (ns.textContent || ns.innerHTML);
            if (!contents || contents.replace(/[\s\t\r\n]+/, "").length == 0) {
                //IE doesn't let us touch noscript, so we have to use attributes.
                var img = new Image();
                for (var ai = 0; ai < ns.attributes.length; ai++) {
                    var a = ns.attributes[ai];
                    if (a && a.specified && a.name.indexOf("data-img-") == 0) {
                        img.setAttribute(a.name.slice(9 - a.name.length), a.value);
                    }
                }
                div.appendChild(img);
            } else {
                //noscript isn't part of DOM, so we have to recreate it, unescaping html, src->data-src
                div.innerHTML = contents.replace(/\s+src\s*=\s*(['"])/i, " data-src=$1").
                    replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
            }
            //Clear source values before we add it back to the dom, ensure data-slimmage is set.
            var childImages = div.getElementsByTagName("img");
            for (var j = 0, jl = childImages.length; j < jl; j++) {
                var ci = childImages[j];
                if (ci.src !== null && ci.src.length > 0) {
                    ci.setAttribute("data-src", ci.src);
                    ci.src = "";
                }
                ci.setAttribute("data-slimmage", "true");
                ns.parentNode.insertBefore(ci, ns);
                newImages++;
            }
            //2. Remove old noscript tags
            ns.parentNode.removeChild(ns);
        }
    }
    return newImages;
}

export var Update = (elt: HTMLElement): number => {
    // Find images with data-slimmage and run adjustImageSrc.
    var totalImages = 0;
    var images = _NodesToArray(elt.getElementsByTagName("img"));
    for (var i = 0, il = images.length; i < il; i++) {
        if (images[i].getAttribute("data-slimmage") !== null) {
            var originalSrc = images[i].getAttribute("data-src") || images[i].src;
            _AdjustImageSrc(images[i], originalSrc);
            totalImages++;
        }
    }
    return totalImages;
}

export var CheckElement = (elt: HTMLElement) => {
    var stopwatch = new Date().getTime();

    var newImages = Replace(elt);

    var totalImages = Update(elt);

    _Log("Slimmage: restored " + newImages + " images from noscript tags; sizing " + totalImages + " images. " + (new Date().getTime() - stopwatch) + "ms");
}

export var Check = (delay?) => {
    if (__timeoutid > 0) clearTimeout(__timeoutid);
    __timeoutid = 0;
    if (delay && delay > 0) {
        __timeoutid = setTimeout(Check, delay);
        return;
    }

    var stopwatch = new Date().getTime();

    var newImages = Replace(document.body);

    var totalImages = Update(document.body);

    _Log("Slimmage: restored " + newImages + " images from noscript tags; sizing " + totalImages + " images. " + (new Date().getTime() - stopwatch) + "ms");
}

var h = Check;
if (OnDomReady) {
    // Run on resize and domready (w.load as a fallback)
    if (addEventListener) {
        addEventListener("DOMContentLoaded", function () {
            h();
            // Run once only
            removeEventListener("load", h, false);
        }, false);
        addEventListener("load", h, false);
    } else if (attachEvent) {
        attachEvent("onload", h);
    }
}

if (OnResize) {
    if (addEventListener) {
        addEventListener("resize", () => { h(500); }, false);
    } else if (attachEvent) {
        attachEvent("onresize", h);
    }
}
//test for webp support
_BeginWebPTest();