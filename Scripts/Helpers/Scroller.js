define(["require", "exports", "Helpers/Helpers", "Helpers/Animator", "Helpers/Easing/EaseOutQuart"], function (require, exports, Helpers, Animator, EaseOutQuart) {
    "use strict";
    var _Animator, _ScrollContainer = Helpers.GetScrollTopElement();
    var _onFrame = function (val) {
        _ScrollContainer.scrollTop = val;
    };
    var _onEnd = function (top) {
        _onFrame(top);
        _Animator = null;
    };
    exports.To = function (val) {
        if (_Animator != null)
            _Animator.Cancel();
        _Animator = new Animator.Engine(_ScrollContainer.scrollTop, val, 30, _onFrame, function (a) { return _onEnd(val); }, EaseOutQuart);
        _Animator.Start();
    };
    exports.ToHtmlElt = function (scrollElt, minusHeader, offset) {
        if (minusHeader === void 0) { minusHeader = false; }
        if (offset === void 0) { offset = 0; }
        var headerHeight = minusHeader ? Helpers.GetHeight(document.getElementById("header")) : 0;
        if (!scrollElt)
            return;
        var top = Helpers.GetTop(scrollElt) - headerHeight - offset;
        exports.To(top);
    };
});
