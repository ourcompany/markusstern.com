define(["require", "exports"], function (require, exports) {
    "use strict";
    /// <reference path="ieasing.d.ts" />
    var EaseOutQuart = function (currentIteration, startValue, changeInValue, totalIterations) {
        return -changeInValue * (Math.pow(currentIteration / totalIterations - 1, 4) - 1) + startValue;
    };
    return EaseOutQuart;
});
