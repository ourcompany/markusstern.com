define(["require", "exports"], function (require, exports) {
    "use strict";
    /// <reference path="ieasing.d.ts" />
    var EaseInOutSine = function (currentIteration, startValue, changeInValue, totalIterations) {
        return changeInValue / 2 * (1 - Math.cos(Math.PI * currentIteration / totalIterations)) + startValue;
    };
    return EaseInOutSine;
});
