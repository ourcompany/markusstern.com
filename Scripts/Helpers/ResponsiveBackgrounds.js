define(["require", "exports", "Helpers/Helpers"], function (require, exports, Helpers) {
    "use strict";
    exports.OnDomReady = true;
    exports.OnResize = true;
    exports.Verbose = false;
    exports.TryWebP = false;
    var _WebP = false;
    var _Log = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (exports.Verbose && window.console && window.console.log)
            try {
                window.console.log.apply(window.console, args);
            }
            catch (e) { }
    };
    var __testingWebP;
    var _BeginWebPTest = function () {
        if (!exports.TryWebP || __testingWebP)
            return;
        __testingWebP = true;
        var WebP = new Image();
        WebP.onload = WebP.onerror = function () {
            _WebP = (WebP.height == 2);
        };
        WebP.src = 'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';
    };
    var _GetCssValue = function (target, hyphenProp) {
        var val = typeof (window.getComputedStyle) != "undefined" && window.getComputedStyle(target, null).getPropertyValue(hyphenProp);
        if (!val && target.currentStyle) {
            val = target.currentStyle[hyphenProp.replace(/([a-z])\-([a-z])/, function (a, b, c) { return b + c.toUpperCase(); })] || target.currentStyle[hyphenProp];
        }
        return val != "none" ? val : null;
    };
    var _WidthRegex = /width=\d+/i, _HeightRegex = /height=\d+/i;
    //Expects virtual, not device pixel width
    var _AdjustImageSrcWithWidth = function (elt, originalSrc, width) {
        var data = {
            webp: _WebP,
            width: width,
            dpr: window.devicePixelRatio || 1,
            requestedWidth: 0,
            quality: 0,
        };
        data.requestedWidth = Math.min(2048, data.width * data.dpr),
            data.quality = (data.dpr > 1.49) ? 80 : 90; //Default quality
        if (_WebP)
            data.quality = data.dpr > 1.49 ? 65 : 78;
        //Minimize variants for caching improvements; round up to nearest multiple of 160
        data.requestedWidth = data.requestedWidth - (data.requestedWidth % 160) + 160; //Will limit to 13 variations
        var oldpixels = parseInt(elt.getAttribute("data-pixel-width")) | 0;
        if (data.requestedWidth > oldpixels) {
            //Never request a smaller image once the larger one has already started loading
            var newSrc = originalSrc.replace(_WidthRegex, "width=" + data.requestedWidth).replace(/quality=[0-9]+/i, "quality=" + data.quality);
            if (_WebP)
                newSrc = newSrc.replace(/format=[a-z]+/i, "format=webp");
            elt.style.backgroundImage = "url(" + newSrc + ")";
            elt.setAttribute("data-pixel-width", data.requestedWidth.toString());
            _Log("Slimming Background: updating " + newSrc);
        }
    };
    var _AdjustImageSrcWithHeight = function (elt, originalSrc, height) {
        var data = {
            webp: _WebP,
            height: height,
            dpr: window.devicePixelRatio || 1,
            requestedHeight: 0,
            quality: 0,
        };
        data.requestedHeight = Math.min(2048, data.height * data.dpr),
            data.quality = (data.dpr > 1.49) ? 80 : 90; //Default quality
        if (_WebP)
            data.quality = data.dpr > 1.49 ? 65 : 78;
        //Minimize variants for caching improvements; round up to nearest multiple of 160
        data.requestedHeight = data.requestedHeight - (data.requestedHeight % 160) + 160; //Will limit to 13 variations
        var oldpixels = parseInt(elt.getAttribute("data-pixel-height")) | 0;
        if (data.requestedHeight > oldpixels) {
            //Never request a smaller image once the larger one has already started loading
            var newSrc = originalSrc.replace(_HeightRegex, "height=" + data.requestedHeight).replace(/quality=[0-9]+/i, "quality=" + data.quality);
            if (_WebP)
                newSrc = newSrc.replace(/format=[a-z]+/i, "format=webp");
            elt.style.backgroundImage = "url(" + newSrc + ")";
            elt.setAttribute("data-pixel-height", data.requestedHeight.toString());
            _Log("Slimming Background: updating " + newSrc);
        }
    };
    var _AdjustImageSrcWithWidthAndHeight = function (elt, originalSrc, width, height) {
        var data = {
            webp: _WebP,
            width: width,
            height: height,
            dpr: window.devicePixelRatio || 1,
            requestedWidth: 0,
            requestedHeight: 0,
            quality: 0,
        };
        data.requestedWidth = Math.min(2048, data.width * data.dpr),
            data.requestedHeight = Math.min(2048, data.height * data.dpr),
            data.quality = (data.dpr > 1.49) ? 80 : 90; //Default quality
        if (_WebP)
            data.quality = data.dpr > 1.49 ? 65 : 78;
        //Minimize variants for caching improvements; round up to nearest multiple of 160
        data.requestedWidth = data.requestedWidth - (data.requestedWidth % 160) + 160; //Will limit to 13 variations
        data.requestedHeight = data.requestedHeight - (data.requestedHeight % 160) + 160; //Will limit to 13 variations
        var oldwidth = parseInt(elt.getAttribute("data-pixel-width")) | 0, oldheight = parseInt(elt.getAttribute("data-pixel-height")) | 0;
        if (data.requestedWidth > oldwidth || data.requestedHeight > oldheight) {
            //Never request a smaller image once the larger one has already started loading
            var newSrc = originalSrc
                .replace(_WidthRegex, "width=" + data.requestedWidth)
                .replace(_HeightRegex, "height=" + data.requestedHeight)
                .replace(/quality=[0-9]+/i, "quality=" + data.quality);
            if (_WebP)
                newSrc = newSrc.replace(/format=[a-z]+/i, "format=webp");
            elt.style.backgroundImage = "url(" + newSrc + ")";
            elt.setAttribute("data-pixel-width", data.requestedWidth.toString());
            elt.setAttribute("data-pixel-height", data.requestedHeight.toString());
            _Log("Slimming Background: updating " + newSrc);
        }
    };
    var _GetCssPixels = function (target, prop) {
        var val = _GetCssValue(target, "max-" + prop) || _GetCssValue(target, prop) || _GetCssValue(target, "min-" + prop);
        //We can return pixels directly, but not other units
        if (val.slice(-2) == "px")
            return parseFloat(val.slice(0, -2));
        //Create a temporary sibling div to resolve units into pixels.
        var temp = document.createElement("div");
        temp.style.overflow = temp.style.visibility = "hidden";
        target.parentNode.appendChild(temp);
        temp.style[prop] = val;
        var pixels = temp["offset" + prop[0].toUpperCase() + prop.substr(1)];
        target.parentNode.removeChild(temp);
        return pixels;
    };
    var _AdjustImageSrc = function (elt, originalSrc) {
        var w = _WidthRegex.test(originalSrc), h = _HeightRegex.test(originalSrc);
        if (w && h)
            _AdjustImageSrcWithWidthAndHeight(elt, originalSrc, _GetCssPixels(elt, "width"), _GetCssPixels(elt, "height"));
        else if (w)
            _AdjustImageSrcWithWidth(elt, originalSrc, _GetCssPixels(elt, "width"));
        else if (h)
            _AdjustImageSrcWithHeight(elt, originalSrc, _GetCssPixels(elt, "height"));
    };
    var __timeoutid = 0;
    exports.Update = function (elt) {
        // Find images with data-slimmage and run adjustImageSrc.
        var totalImages = 0;
        var images = Helpers.NodeListToArray(elt.getElementsByClassName("responsive-backgrounds"));
        for (var i = 0, il = images.length; i < il; i++) {
            if (images[i].getAttribute("data-src") !== null) {
                var originalSrc = images[i].getAttribute("data-src");
                _AdjustImageSrc(images[i], originalSrc);
                totalImages++;
            }
        }
        return totalImages;
    };
    exports.Check = function (delay) {
        if (__timeoutid > 0)
            clearTimeout(__timeoutid);
        __timeoutid = 0;
        if (delay && delay > 0) {
            __timeoutid = setTimeout(exports.Check, delay);
            return;
        }
        var stopwatch = new Date().getTime();
        var totalImages = exports.Update(document.body);
        _Log("Slimmage: sizing " + totalImages + " images. " + (new Date().getTime() - stopwatch) + "ms");
    };
    var h = exports.Check;
    if (exports.OnDomReady) {
        // Run on resize and domready (w.load as a fallback)
        if (addEventListener) {
            addEventListener("DOMContentLoaded", function () {
                h();
                // Run once only
                removeEventListener("load", h, false);
            }, false);
            addEventListener("load", h, false);
        }
        else if (attachEvent) {
            attachEvent("onload", h);
        }
    }
    if (exports.OnResize) {
        if (addEventListener) {
            addEventListener("resize", function () { h(500); }, false);
        }
        else if (attachEvent) {
            attachEvent("onresize", h);
        }
    }
    //test for webp support
    _BeginWebPTest();
});
