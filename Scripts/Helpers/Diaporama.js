define(["require", "exports", "Helpers/Animator", "Helpers/Easing/Linear"], function (require, exports, Animator, LinearEase) {
    "use strict";
    var Diaporama = (function () {
        function Diaporama(slides, options) {
            this.Current = -1;
            this._Frame = 0;
            this._Animator = {};
            if (!slides.length)
                return;
            this._Slides = slides;
            this._Delay = options.delay || 6000;
            this._Duration = options.duration || 120;
            this._Easings = options.easings || { show: LinearEase, hide: LinearEase };
            this._Css = options.css;
            this._On = options.on;
            if (options.css && options.css.show) {
                var l = slides.length;
                while (l--) {
                    if (slides[l].classList.contains(options.css.show))
                        this.Current = l;
                    break;
                }
            }
            if (options.autostart)
                this.Play();
            else if (this.Current < 0) {
                this.Current = -1;
                this._Next();
            }
        }
        Diaporama.prototype.Play = function () {
            this._Playing = true;
            // add interval if not animating nor already existing.
            this._Next();
        };
        Diaporama.prototype.Pause = function () {
            this._Playing = false;
            // remove interval already existing.
        };
        Diaporama.prototype.Go = function (slideNum) {
            console.log(slideNum);
            this.Pause();
            this._Go(slideNum);
        };
        Diaporama.prototype.Next = function () {
            this.Pause();
            this._Next();
        };
        Diaporama.prototype.Previous = function () {
            this.Pause();
            this._Previous();
        };
        Diaporama.prototype._Go = function (slideNum) {
            var oldElt = this._Slides[this.Current], oldNum = this.Current, newElt = this._Slides[slideNum];
            this.Current = slideNum;
            this._Animate(oldElt, oldNum, newElt, this.Current);
        };
        Diaporama.prototype._Animate = function (oldElt, oldNum, newElt, newNum) {
            var _this = this;
            if (oldElt) {
                var oldFrom = oldElt.style.opacity ? parseFloat(oldElt.style.opacity) : 1;
                if (this._Animator[oldNum]) {
                    this._Animator[oldNum].Cancel();
                    delete this._Animator[oldNum];
                }
                if (oldFrom !== 0 && this._Easings.hide) {
                    this._Animator[oldNum] = new Animator.Engine(oldFrom, 0, this._Duration, function (val) { return _this._OnFrame(oldNum, val); }, function (animator) { return _this._OnEnd(oldNum, animator, 0); }, this._Easings.hide);
                    this._Animator[oldNum].Start();
                }
                else
                    this._OnSlideHide(oldNum, oldElt);
                this._Hide(oldElt, oldNum);
            }
            if (newElt) {
                var newFrom = newElt.style.opacity ? parseFloat(newElt.style.opacity) : 0;
                if (this._Animator[newNum]) {
                    this._Animator[newNum].Cancel();
                    delete this._Animator[newNum];
                }
                if (this._On && this._On.show) {
                    this._On.beforeshow(newNum, newElt);
                }
                if (newFrom !== 1 && this._Easings.show) {
                    this._Animator[newNum] = new Animator.Engine(newFrom, 1, this._Duration, function (val) { return _this._OnFrame(newNum, val); }, function (animator) { return _this._OnEnd(newNum, animator, 1); }, this._Easings.show);
                    this._Animator[newNum].Start();
                }
                else
                    this._OnSlideShow(newNum, newElt);
                this._Show(newElt, newNum);
            }
        };
        Diaporama.prototype._OnFrame = function (index, val) {
            this._Slides[index].style.opacity = val.toString();
        };
        Diaporama.prototype._OnEnd = function (index, animator, val) {
            var slide = this._Slides[index];
            slide.style.opacity = val.toString();
            this._Animator[index].Cancel();
            delete this._Animator[index];
            if (val === 1) {
                this._OnSlideShow(index, slide);
            }
            else if (val === 0) {
                this._OnSlideHide(index, slide);
            }
        };
        Diaporama.prototype._OnSlideShow = function (index, slide) {
            if (this._Playing && this._Slides.length > 1) {
                clearTimeout(this._PlayingInte);
                this._PlayingInte = setTimeout(this._Next.bind(this), this._Delay);
            }
            if (this._On && this._On.show) {
                this._On.show(index, slide);
            }
        };
        Diaporama.prototype._OnSlideHide = function (index, slide) {
            if (this._On && this._On.hide) {
                this._On.hide(index, slide);
            }
        };
        Diaporama.prototype._Next = function () {
            this._Go((this.Current + 1) % this._Slides.length);
        };
        Diaporama.prototype._Previous = function () {
            this._Go((this.Current - 1 + this._Slides.length) % this._Slides.length);
        };
        Diaporama.prototype._Show = function (slideElt, slideNum) {
            if (this._Css && this._Css.show)
                slideElt.classList.add(this._Css.show);
            if (this._Css && this._Css.hide)
                slideElt.classList.remove(this._Css.hide);
        };
        Diaporama.prototype._Hide = function (slideElt, slideNum) {
            if (this._Css && this._Css.show)
                slideElt.classList.remove(this._Css.show);
            if (this._Css && this._Css.hide)
                slideElt.classList.add(this._Css.hide);
        };
        Diaporama.prototype.Destroy = function () {
            if (this._PlayingInte)
                clearTimeout(this._PlayingInte);
            for (var i in this._Animator) {
                if (this._Animator.hasOwnProperty(i)) {
                    this._Animator[i].Cancel();
                }
            }
            this._Animator = null;
        };
        return Diaporama;
    }());
    return Diaporama;
});
