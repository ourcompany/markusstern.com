﻿/**
 * Enum for Platfomrs
 * @enum {number} - flag values
 */
export const enum Platforms {
    Mobile = 0,
    Tablet = 1,
    Laptop = 2,
    Desktop = 4
}

/**
 * The scripts has been loaded
 * @enum {number} - flag values
 */


export var Platform: Platforms = Platforms.Desktop;
