define(["require", "exports"], function (require, exports) {
    "use strict";
    /**
     * The scripts has been loaded
     * @enum {number} - flag values
     */
    exports.Platform = 4 /* Desktop */;
});
